﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using UnityEngine.SocialPlatforms;
//using System.Net.NetworkInformation;
//using System.Diagnostics;

public class testscript : MonoBehaviour,RealTimeMultiplayerListener
{
    public GameObject signin;
    public GameObject randomplayers;
    public GameObject friends;
    public GameObject maincanvas;
    public GameObject playcanvas;

    public Text loading;
    public static PlayGamesPlatform platform;
    public GameObject player1;
    public Text player1text;
    public GameObject player2;
    public Text player2text;

    public GameObject player;

    public GameObject ball;

    public GameObject win;
    public GameObject lose;

    // Start is called before the first frame update
    void Start()
    {
        win.SetActive(false);
        lose.SetActive(false);

        ball.SetActive(false);

        player1text.text = " " + ToString();
        player2text.text = " " + ToString();

        loading.enabled = false;

        signin.SetActive(true);
        randomplayers.SetActive(false);
        friends.SetActive(false);
        maincanvas.SetActive(true);
        playcanvas.SetActive(false);

            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            platform = PlayGamesPlatform.Activate();
        
        //Social.localUser.Authenticate((bool success) =>
        //{
        //    if (success == true)
        //    {
        //        signin.SetActive(false);
        //        randomplayers.SetActive(true);
        //        friends.SetActive(true);
        //        Debug.Log("Log in succesfully");
        //    }
        //    else
        //    {
        //        signin.SetActive(true);
        //        randomplayers.SetActive(false);
        //        friends.SetActive(false);
        //        Debug.Log("Log in failed");
        //    }
        //});
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Signinpress()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success == true)
            {
                signin.SetActive(false);
                randomplayers.SetActive(true);
                friends.SetActive(true);
            }
            else
            {
                signin.SetActive(true);
                randomplayers.SetActive(false);
                friends.SetActive(false);
            }
        });
    }

    public void CreateQuickGame()
    {
        maincanvas.SetActive(false);
        playcanvas.SetActive(true);
        loading.enabled = true;

        const int MinOpponents = 1, MaxOpponents = 1;
        const int GameType = 0;

        PlayGamesPlatform.Instance.RealTime.CreateQuickGame(MinOpponents, MaxOpponents, GameType, this);
    }

    public void CreateWithInvitationScreen()
    {
        maincanvas.SetActive(false);
        playcanvas.SetActive(true);
        loading.enabled = true;

        const int MinOpponents = 1, MaxOpponents = 1;
        const int GameType = 0;

        PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen(MinOpponents, MaxOpponents, GameType, this);
    }

    public void mainmenupress()
    {
        signin.SetActive(false);
        randomplayers.SetActive(true);
        friends.SetActive(true);
        maincanvas.SetActive(true);
        playcanvas.SetActive(false);
    }

    public void OnPeersConnected(string[] participantIds)
    {
        throw new NotImplementedException();
    }

    public void OnParticipantLeft(Participant participant)
    {
        mainmenupress();
    }

    public void OnPeersDisconnected(string[] participantIds)
    {
        mainmenupress();
    }

    public void OnLeftRoom()
    {
        mainmenupress();
    }

    public void OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
    {
        string position = System.Text.Encoding.Default.GetString(data);
        string[] raw = position.Split(new string[] { ":" }, System.StringSplitOptions.RemoveEmptyEntries);
        if (!isReliable)
        {
            if (raw[0] == "ball")
            {
                ball.transform.position = new Vector2(System.Convert.ToSingle(raw[1]), System.Convert.ToSingle(raw[2]));
            }
            if (raw[0] == "player1")
            {
                ball.transform.position = new Vector2(System.Convert.ToSingle(raw[1]), System.Convert.ToSingle(raw[2]));
            }
            if (raw[0] == "player2")
            {
                ball.transform.position = new Vector2(System.Convert.ToSingle(raw[1]), System.Convert.ToSingle(raw[2]));
            }
            else
            {
                if (raw[0] == "0win")
                {
                    righthit();
                }
                if (raw[0] == "0lose")
                {
                    lefthit();
                }
            }
        }
    }
    public void OnRoomSetupProgress(float progress)
    {
        loading.text = progress.ToString();
    }

    public void OnRoomConnected(bool success)
    {
        loading.text = " ";
        Participant myself = PlayGamesPlatform.Instance.RealTime.GetSelf();
        List<Participant> participants = PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants();
        player1text.text = participants[0].DisplayName;
        player2text.text = participants[1].DisplayName;
        ball.SetActive(true);
        if(myself.ParticipantId == participants[0].ParticipantId)
        {
            player = player1;
            ball.GetComponent<ballscript>().enabled = true;
        }
        else if (myself.ParticipantId == participants[1].ParticipantId)
        {
            player = player2;
            ball.GetComponent<ballscript>().enabled = false;
        }

    }

    public void lefthit()
    {
        if (player == player1)
        {
            win.SetActive(false);
            lose.SetActive(true);

            string lost = "0lose";
            byte[] lostarray = System.Text.ASCIIEncoding.Default.GetBytes(lost);
            PlayGamesPlatform.Instance.RealTime.SendMessageToAll(true, lostarray);
        }
        else
        {
            win.SetActive(true);
            lose.SetActive(false);
        }
    }

    public void righthit()
    {
        if (player == player1)
        {
            win.SetActive(true);
            lose.SetActive(false);
        }
        else
        {
            win.SetActive(false);
            lose.SetActive(true);
        }
    }
    public void up() //gerak player keatas
    {
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 150 * Time.deltaTime);
    }

    public void down()//gerak player kebawah
    {
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -150 * Time.deltaTime);
    }

    public void velocityzero()//player diam
    {
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }
}
