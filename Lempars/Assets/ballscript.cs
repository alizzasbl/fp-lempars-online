﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;

public class ballscript : MonoBehaviour
{
    private ballscript balls;
    private testscript script;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        script = GameObject.Find("script").GetComponent<testscript>();
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(-100 * Time.deltaTime, 0);
    }

    void OnCollisionEnter2D(Collision2D col) //bola collision dg obj dalam game (player&dinding luar)
    {
        if (col.gameObject.name == "player1")
        {
            rb.velocity = new Vector2(100 * Time.deltaTime, gameObject.transform.position.y * 2 - col.transform.position.y * 2);
        }
        if (col.gameObject.name == "player2")
        {
            rb.velocity = new Vector2(-100 * Time.deltaTime, gameObject.transform.position.y * 2 - col.transform.position.y * 2);
        }
        if (col.gameObject.name == "leftwall")
        {
            script.lefthit();
        }
        if (col.gameObject.name == "rightwall")
        {
            script.righthit();
        }
    }

    void Update()
    {
        if (script.player == script.player1)
        {
            string data = "ball" + ":" + balls.transform.position.x + ":" + balls.transform.position.y;
            byte[] bytedata = System.Text.ASCIIEncoding.Default.GetBytes(data);
            PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, bytedata);

            string player1data = "player1" + ":" + script.player1.transform.position.x + ":" + script.player1.transform.position.y;
            byte[] player1dataarray = System.Text.ASCIIEncoding.Default.GetBytes(player1data);
            PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, player1dataarray);
        }
        else if (script.player == script.player2)
        {
            string player2data = "player2" + ":" + script.player2.transform.position.x + ":" + script.player2.transform.position.y;
            byte[] player2dataarray = System.Text.ASCIIEncoding.Default.GetBytes(player2data);
            PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, player2dataarray);
        }
        else 
        {
            
        }
    }
}


